const div = document.querySelector("div");
var sort = document.createElement("button");
//sort.setAttribute("onclick", superSort());
sort.textContent = "Sort";
div.appendChild(sort);
var requestURL1 = "https://rickandmortyapi.com/api/character/";
var requestURL2 = "https://rickandmortyapi.com/api/character/?page=2";
var requestURL3 = "https://rickandmortyapi.com/api/character/?page=3";
function getCharacter(requestURL) {
  var request = new XMLHttpRequest();
  request.open("GET", requestURL);
  request.responseType = "json";
  request.send();
  request.onload = function () {
    var character = request.response;
    showCharacter(character);
  };
}

getCharacter(requestURL1);
getCharacter(requestURL2);
getCharacter(requestURL3);

function showCharacter(jsonObj) {
  var characters = jsonObj["results"];

  for (var i = 0; i < characters.length; i++) {
    var myArticle = document.createElement("article");
    myArticle.setAttribute("id", [i]);
    var myH2 = document.createElement("h2");
    var img = document.createElement("img");
    var myPara1 = document.createElement("p");
    var myPara2 = document.createElement("p");
    var myPara3 = document.createElement("p");
    var myList = document.createElement("ul");
    var close = document.createElement("button");
    close.setAttribute("onclick", hideCharacter);

    myH2.textContent = characters[i].name;
    img.src = characters[i].image;
    myPara1.textContent = "Species: " + characters[i].species;
    myPara2.textContent = "Location: " + characters[i].location.name;
    myPara3.textContent = "Was created: " + characters[i].created;
    close.textContent = "close";

    var Episodes = characters[i].episode;
    for (var j = 0; j < Episodes.length; j++) {
      var listItem = document.createElement("li");
      listItem.textContent = Episodes[j];
      myList.appendChild(listItem);
    }

    myArticle.appendChild(myH2);
    myArticle.appendChild(img);
    myArticle.appendChild(myPara1);
    myArticle.appendChild(myPara2);
    myArticle.appendChild(myPara3);
    myArticle.appendChild(myList);
    myArticle.appendChild(close);

    div.appendChild(myArticle);

    function hideCharacter() {
      document.getElementById([i]).style.display = "none";
      //console.log(document.getElementById([i]));
    }
  }
}
